import React, { Component } from 'react';

import Button from 'components/Button';
import Input from 'components/Form/Input';

import { firebaseApp } from 'config/firebase';

import css from 'style/containers/Login';

export default class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      error: '',
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = this.state;

    firebaseApp.auth().signInWithEmailAndPassword(email, password)
      .catch(error => this.setState({ error: error.message }));

    this.setState({
      email: '',
      password: '',
    });
  }

  render() {
    const { email, password, error } = this.state;

    return (
      <section>
        <form className={ css.formGroup } onSubmit={ this.handleSubmit }>
          <div className={ css.formGroup__item }>
            <Input label='Email' name='email' type='text' value={ email } onChange={ this.handleChange } />
          </div>
          <div className={ css.formGroup__item }>
            <Input label='Password' name='password' type='password' value={ password } onChange={ this.handleChange } />
          </div>
          <div className={ css.formGroup__button }>
            <Button type='submit' className={ css.form__button } primaryClass={ true }>SignIn</Button>
          </div>
        </form>
        <div className={ css.formGroup__error }>
          { error }
        </div>
      </section>
    );
  }
}
