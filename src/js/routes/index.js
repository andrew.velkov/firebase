import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from 'routes/history';
import App from 'containers/App';

import Main from 'pages/Main';
import NotFound from 'pages/NotFound';

import { firebaseApp } from 'config/firebase';

export default class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }

  componentDidMount() {
    this.authListener();
  }

  authListener = () => {
    firebaseApp.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ data: { user: !user || true, email: user.email } });
      } else {
        this.setState({ data: { user: null } });
      }
    });
  }

  render() {
    const { data } = this.state;

    return (
      <Router history={ history }>
        <App>
          <Switch>
            <Route exact path='/' render={ (props) => <Main auth={ data } { ...props } /> } />
            <Route component={ NotFound } />
          </Switch>
        </App>
      </Router>
    );
  }
}
