const apiMiddleWare = () => next => action => {
  const { types, request: { method = 'GET', url, body }, status, ...rest } = action;

  if (!types) {
    return next(action);
  }

  const [REQUEST, SUCCESS, FAILURE] = types;

  next({ ...rest, type: REQUEST });

  return fetch(encodeURI(url), {
    method,
    body,
  })
    .then(res => {
      return res.json();
    })
    .then(data => next({
      ...rest,
      type: SUCCESS,
      payload: data,
    }))
    .catch(error => next({
      ...rest,
      type: FAILURE,
      payload: error.message,
    }));
};

export default apiMiddleWare;
