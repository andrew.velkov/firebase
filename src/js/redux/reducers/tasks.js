import md5 from 'md5';

const GET_TASK = 'TASK/GET_TASK';
const GET_TASK_SUCCESS = 'TASK/GET_TASK_SUCCESS';
const GET_TASK_ERROR = 'TASK/GET_TASK_ERROR';

const CREATE_TASK = 'TASK/CREATE_TASK';
const CREATE_TASK_SUCCESS = 'TASK/CREATE_TASK_SUCCESS';
const CREATE_TASK_ERROR = 'TASK/CREATE_TASK_ERROR';

const EDIT_TASK = 'TASK/EDIT_TASK';
const EDIT_TASK_SUCCESS = 'TASK/EDIT_TASK_SUCCESS';
const EDIT_TASK_ERROR = 'TASK/EDIT_TASK_ERROR';

const initialState = {
  get: {
    loaded: false,
    loading: false,
    data: [],
  },
  create: {
    loaded: false,
    loading: false,
    status: '',
  },
  edit: {
    loaded: false,
    loading: false,
    status: '',
  },
};

export default function postReducer(state = initialState, action) {
  switch (action.type) {
    case GET_TASK:
      return {
        ...state,
        get: {
          ...state.get,
          loading: true,
          loaded: false,
        },
      };
    case GET_TASK_SUCCESS:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: true,
          data: action.payload.message.tasks,
          total: action.payload.message.total_task_count,
        },
      };
    case GET_TASK_ERROR:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: false,
        },
      };
    case CREATE_TASK:
      return {
        ...state,
        create: {
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case CREATE_TASK_SUCCESS:
      return {
        ...state,
        create: {
          loading: false,
          loaded: true,
          data: action.payload,
          status: action.payload.status,
        },
      };
    case CREATE_TASK_ERROR:
      return {
        ...state,
        create: {
          loading: false,
          status: action.payload.status,
        },
      };
    case EDIT_TASK:
      return {
        ...state,
        edit: {
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case EDIT_TASK_SUCCESS:
      return {
        ...state,
        edit: {
          loading: false,
          loaded: true,
          status: action.payload.status,
        },
      };
    case EDIT_TASK_ERROR:
      return {
        ...state,
        edit: {
          loading: false,
          status: action.payload,
        },
      };
    default:
      return state;
  }
}

export const getTask = (page, sortField = 'id', sortDirection = 'asc') => {
  const currentPage = page || 1;

  return {
    types: [GET_TASK, GET_TASK_SUCCESS, GET_TASK_ERROR],
    request: {
      method: 'GET',
      url: `https://uxcandy.com/~shapoval/test-task-backend/?developer=andrew&page=${ currentPage }&sort_field=${ sortField }&sort_direction=${ sortDirection }`,
    },
  };
};

export const createTask = data => {
  return {
    types: [CREATE_TASK, CREATE_TASK_SUCCESS, CREATE_TASK_ERROR],
    request: {
      method: 'POST',
      url: 'https://uxcandy.com/~shapoval/test-task-backend/create?developer=andrew',
      body: data,
    },
    status: 'Created!',
  };
};

export const editTask = (id, fields) => {
  const chars = { ':': '=', ',': '&', '{': '', '}': '', '"': '' };

  const paramsString = JSON.stringify(fields).replace(/[:,{}"]/g, m => chars[m]);
  const signature = md5(encodeURIComponent(paramsString).replace(/%3D/g, '=').replace(/%26/g, '&'));
  const data = { signature, ...fields };
  const formData = new FormData();

  /* eslint-disable */
  for (const name in data) {
    if (data.hasOwnProperty(name)) {
      formData.append(name, data[name]);
    }
  }

  return {
    types: [EDIT_TASK, EDIT_TASK_SUCCESS, EDIT_TASK_ERROR],
    request: {
      method: 'POST',
      url: `https://uxcandy.com/~shapoval/test-task-backend/edit/${ id }?developer=andrew`,
      body: formData,
    },
    status: 'Edited Succes!',
  };
};
