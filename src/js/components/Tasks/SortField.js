import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';

import css from 'style/pages/Main';

const SortField = ({ field, setSort, columnToSort, order }) => {
  const isSort = field.value === columnToSort;

  return (
    <li className={ css.sortList__item }>
      <Link
        className={ cx({ [css.arrow]: isSort, [css[`arrow_${ order }`]]: true }) }
        to=''
        onClick={ setSort }
      >
        { field.value }
      </Link>
    </li>
  );
};

SortField.propTypes = {
  field: PropTypes.object,
  setSort: PropTypes.func,
  order: PropTypes.string,
  columnToSort: PropTypes.string,
};

export default SortField;
