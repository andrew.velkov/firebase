import React from 'react';
import PropTypes from 'prop-types';

import EditTask from 'components/Tasks/Edit';

import css from 'style/pages/Main';

const Item = ({ data, auth }) => {
  const text = data.text && data.text.split('\n').map(item => <span key={ `id_${ Math.floor(Math.random() * data.id) + 5 }` }>{ item }<br /></span>);
  const dataImage = data.image_path || data.imagePreviewUrl;
  let status;

  if (!data.status) {
    status = 'Task failed';
  } else if (data.status === 0) {
    status = 'Task failed';
  } else {
    status = 'Task completed';
  }

  return (
    <li className={ css.task__item }>
      {dataImage && <img className={ css.task__image } src={ dataImage } alt='' />}
      <div>
        <p><b>Username:</b> { data.username }</p>
        <p><b>Email:</b> { data.email }</p>
        <p><b>Status:</b> { status }</p>
        <p><b>Description:</b> { text }</p>
      </div>
      {auth && <div className={ css.task__actions }>
        <EditTask task={ data } />
      </div>}
    </li>
  );
};

Item.propTypes = {
  data: PropTypes.object,
  auth: PropTypes.bool,
};

export default Item;
