import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { createTask } from 'redux/reducers/tasks';
import Button from 'components/Button';
import Dialog from 'components/Dialog';
import Input from 'components/Form/Input';
import Preview from 'components/Tasks/Item';

import css from 'style/pages/Main';

@connect(state => ({
  create: state.tasks.create,
}),
{ createTask }
)
export default class Add extends Component {
  static propTypes = {
    create: PropTypes.object,
    createTask: PropTypes.func,
    fetching: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      text: '',
      image: '',
      imagePreviewUrl: '',
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleImageChange = (e) => {
    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({ imagePreviewUrl: reader.result });
    };

    if (file) {
      reader.readAsDataURL(file);
    } else {
      this.setState({ imagePreviewUrl: '' });
    }
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    const data = new FormData(this.form);

    await this.props.createTask(data)
      .then(res => res.payload.status === 'ok' && this.setState({ username: '', email: '', text: '' }));
    this.props.fetching();
  }

  render() {
    const { status } = this.props.create;
    const { username, email, text } = this.state;
    const message = status === 'error' && this.props.create.data.message;
    const isValidate = username === '';

    return (
      <form className={ css.form } encType='multipart/form-data' ref={ el => (this.form = el) } onSubmit={ this.handleSubmit }>
        <h3 className={ css.form__title }>Add Task</h3>
        <Input
          type='text'
          label='Username'
          name='username'
          value={ username }
          error={ message.username }
          onChange={ (e) => this.handleChange(e) }
        />
        <Input
          type='email'
          label='Email'
          name='email'
          value={ email }
          error={ message.email }
          onChange={ (e) => this.handleChange(e) }
        /><br /><br /><br />
        <Input
          name='image'
          type='file'
          accept='image/*'
          error={ message.image }
          onChange={ (e) => this.handleImageChange(e) }
        />
        <Input
          type='text'
          label='Text'
          name='text'
          value={ text }
          multiLine={ true }
          error={ message.text }
          onChange={ (e) => this.handleChange(e) }
        />

        <ul className={ cx(css.buttonGroup, css.buttonGroup_between) }>
          <li className={ css.buttonGroup__item }>
            <Button type='submit' primaryClass={ true } disabled={ isValidate }>Send</Button>
          </li>
          {!isValidate && <li className={ css.buttonGroup__item }>
            <Dialog title='Preview Task' buttonName='Preview' buttonType='button' buttonSend={ false } secondaryClass={ true }>
              <Preview data={ this.state } />
            </Dialog>
          </li>}
        </ul>
      </form>
    );
  }
}
