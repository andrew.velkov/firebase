import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getTask, editTask } from 'redux/reducers/tasks';
import Dialog from 'components/Dialog';
import Input from 'components/Form/Input';
import Select from 'components/Form/Select';

import statusConfig from 'config/status';

import css from 'style/pages/Main';

@connect(() => ({}), { getTask, editTask })
export default class Edit extends Component {
  static propTypes = {
    task: PropTypes.object,
    getTask: PropTypes.func,
    editTask: PropTypes.func,
  };

  constructor(props) {
    super(props);

    const { status, text } = this.props.task;
    this.state = { status, text };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleChangeSelect = (e, index, data) => {
    this.setState({
      status: data.value,
    });
  }

  handleEditTask = async (e, id) => {
    e.preventDefault();

    const { status, text } = this.state;
    const fields = { status: Number(status), text, token: 'beejee' };

    await this.props.editTask(id, fields);
    this.props.getTask();
  }

  render() {
    const { id, username, email } = this.props.task;
    const { status, text } = this.state;

    return (
      <Dialog buttonType='icon' buttonName='edit' primaryClass={ true } onClick={ (e) => this.handleEditTask(e, id) }>
        <form encType='multipart/form-data' ref={ el => (this.form = el) }>
          <h3 className={ css.form__title }>Edit Task / { username } / { email }</h3>
          <Select
            fullWidthBool={ true }
            label='Status'
            data={ statusConfig }
            value={ String(status) }
            handleChange={ this.handleChangeSelect }
          />
          <Input
            type='text'
            label='Text'
            name='text'
            value={ text }
            multiLine={ true }
            onChange={ (e) => this.handleChange(e) }
          />
        </form>
      </Dialog>
    );
  }
}
