import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Button from 'components/Button';
import Login from 'containers/Login';

import { firebaseApp } from 'config/firebase';

import css from 'style/components/Header';

export default class Header extends Component {
  static propTypes = {
    auth: PropTypes.object,
  };

  signOut = () => {
    firebaseApp.auth().signOut();
  }

  login = () => <Login />;

  logout = () => {
    const { auth: { email } } = this.props;

    return (
      <ul className={ cx(css.buttonGroup, css.buttonGroup_between) }>
        <li className={ css.buttonGroup__item }>
          { email }
        </li>
        <li className={ css.buttonGroup__item }>
          <Button secondaryClass={ true } onClick={ () => this.signOut() }>Logout</Button>
        </li>
      </ul>
    );
  }

  render() {
    const { auth } = this.props;
    const isAdmin = auth.user ? this.logout() : this.login();

    return (
      <header className={ css.header }>
        <div className={ css.header__login }>
          { isAdmin }
        </div>
      </header>
    );
  }
}
