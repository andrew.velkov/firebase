import React, { Component } from 'react';

import css from 'style/components/ImageUploader';

export default class ImageUploader extends Component {
  constructor(props) {
    super(props);
    this.state = { file: '', imagePreviewUrl: '' };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    console.log('handle uploading-', this.state.file);
  }

  handleImageChange = (e) => {
    e.preventDefault();

    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file,
        imagePreviewUrl: reader.result,
      });
    };

    reader.readAsDataURL(file);
  }

  render() {
    const { imagePreviewUrl } = this.state;
    let $imagePreview = null;

    if (imagePreviewUrl) {
      $imagePreview = `<img src={ ${ imagePreviewUrl } } />`;
    } else {
      $imagePreview = (<div className={ css.previewText }>Please select an Image for Preview</div>);
    }

    return (
      <div className={ css.previewComponent }>
        <form onSubmit={ (e) => this.handleSubmit(e) }>
          <input className={ css.fileInput } type='file' onChange={ (e) => this.handleImageChange(e) } />
          <button className={ css.submitButton } type='submit' onClick={ (e) => this.handleSubmit(e) }>Upload Image</button>
        </form>
        <div className={ css.imgPreview }>{ $imagePreview }</div>
      </div>
    );
  }
}
