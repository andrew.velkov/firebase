import React from 'react';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';

import css from 'style/components/Pagination';

const Pagination = ({ pageCount, handleSetPage }) => (
  <section className={ css.pagination }>
    <div className={ css.pagination__wrap }>
      <div className={ css.pagination__item }>
        <ReactPaginate
          previousLabel={ 'prev ' }
          nextLabel={ 'next' }
          breakLabel={ '...' }
          pageCount={ pageCount }
          marginPagesDisplayed={ 3 }
          pageRangeDisplayed={ 8 }
          onPageChange={ ({ selected }) => handleSetPage(selected + 1) }
        />
      </div>
    </div>
  </section>
);

Pagination.propTypes = {
  handleSetPage: PropTypes.func,
  pageCount: PropTypes.number,
};

export default Pagination;
