const invertDirection = {
  'asc': 'desc',
  'desc': 'asc',
};

export default invertDirection;
