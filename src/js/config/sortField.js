const sortField = [
  {
    id: 1,
    value: 'id',
  }, {
    id: 2,
    value: 'username',
  }, {
    id: 3,
    value: 'email',
  }, {
    id: 4,
    value: 'status',
  },
];

export default sortField;
