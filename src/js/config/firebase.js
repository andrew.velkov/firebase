import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyACCfR5fpTc3Zizhnf-he0ZPH6HM_lSOWw',
  authDomain: 'firetodo-f4199.firebaseapp.com',
  databaseURL: 'https://firetodo-f4199.firebaseio.com',
  projectId: 'firetodo-f4199',
  storageBucket: '',
  messagingSenderId: '986561361354',
};

export const firebaseApp = firebase.initializeApp(config);
