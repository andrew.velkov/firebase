import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { getTask } from 'redux/reducers/tasks';
import Header from 'components/Header';
import Fetching from 'components/Fetching';
import Pagination from 'components/Pagination';

import AddTask from 'components/Tasks/Add';
import Item from 'components/Tasks/Item';
import SortField from 'components/Tasks/SortField';

import invertDirection from 'config/invertDirection';
import sortField from 'config/sortField';

import css from 'style/pages/Main';

@connect(state => ({
  tasks: state.tasks.get,
}),
{ getTask }
)
export default class Main extends Component {
  static propTypes = {
    tasks: PropTypes.object,
    getTask: PropTypes.func,
    auth: PropTypes.object,
  };

  state = {
    selectedPage: 1,
    columnToSort: '',
    sortDirection: 'asc',
  }

  componentDidMount() {
    this.props.getTask();
  }

  setPagination = (selectedPage) => {
    this.props.getTask(selectedPage);

    this.setState({
      selectedPage,
    });
  };

  setSort = (e, sortField) => {
    e.preventDefault();
    const { selectedPage, sortDirection } = this.state;

    this.setState(state => ({
      columnToSort: sortField,
      sortDirection: state.columnToSort === sortField ? invertDirection[state.sortDirection] : 'desc',
    }));

    this.props.getTask(selectedPage, sortField, sortDirection);
  }

  render() {
    const { tasks: { data, loading, total }, auth } = this.props;
    const { sortDirection, columnToSort } = this.state;

    return (
      <section className={ css.wrap }>
        <div className={ css.wrap__container }>
          <div className={ css.wrap__item }>
            <Header auth={ auth } />
          </div>
          <div className={ cx(css.wrap__item, css.wrap__item_grid) }>
            <AddTask fetching={ this.props.getTask } />
            <div>
              <div className={ css.task }>
                <ul className={ css.sortList }>
                  {sortField.map(field => {
                    return (
                      <SortField
                        key={ field.id }
                        field={ field }
                        columnToSort={ columnToSort }
                        order={ sortDirection }
                        setSort={ (e) => this.setSort(e, field.value) }
                      />
                    );
                  })}
                </ul>
                <Fetching size={ 36 } thickness={ 9 } isFetching={ loading }>
                  <ul className={ css.task__list }>
                    {data.map(task => {
                      return (
                        <Item
                          key={ task.id }
                          data={ task }
                          auth={ auth.user }
                        />
                      );
                    })}
                  </ul>
                </Fetching>
              </div>
              <div className={ css.pagination }>
                <Pagination
                  pageCount={ total / 3 }
                  handleSetPage={ this.setPagination }
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
