# Todo List with Firebase Authentication

![alt text](http://i.piccy.info/i9/e22680cc6f34ff866dcda2af6ffbb346/1547952091/55991/1289884/Screenshot_1.png)

```
$ git clone https://gitlab.com/andrew.velkov/firebase
```

```
$ cd firebase
```

```
$ yarn install
```

For start server with mockups:
```
$ yarn start
```

Visit `http://localhost:3003/` 
